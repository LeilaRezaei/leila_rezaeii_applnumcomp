function varargout = BeeAttraction(varargin)
% BEEATTRACTION MATLAB code for BeeAttraction.fig
%      BEEATTRACTION, by itself, creates a new BEEATTRACTION or raises the existing
%      singleton*.
%
%      H = BEEATTRACTION returns the handle to a new BEEATTRACTION or the handle to
%      the existing singleton*.
%
%      BEEATTRACTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BEEATTRACTION.M with the given input arguments.
%
%      BEEATTRACTION('Property','Value',...) creates a new BEEATTRACTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BeeAttraction_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BeeAttraction_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BeeAttraction

% Last Modified by GUIDE v2.5 12-Nov-2019 14:14:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BeeAttraction_OpeningFcn, ...
                   'gui_OutputFcn',  @BeeAttraction_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BeeAttraction is made visible.
function BeeAttraction_OpeningFcn(hObject, eventdata, handles, varargin)

myImage = imread('F:\C.A.5\beefiles/Beeimage.jpg');
set(handles.axes4,'Units','pixels');
resizePos = get(handles.axes4,'Position');
myImage= imresize(myImage, [resizePos(3) resizePos(3)]);
axes(handles.axes4);
imshow(myImage);
set(handles.axes4,'Units','normalized');

% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BeeAttraction (see VARARGIN)

% Choose default command line output for BeeAttraction
handles.output = hObject;

handles.attraction = 0.0; %no attraction
handles.totalTimePoints = 150;% number of time points
handles.output = hObject;
addpath(genpath('beefiles'))

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes BeeAttraction wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = BeeAttraction_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function edit1_Callback(hObject, eventdata, handles)
handles.attraction = enteredvalue/100
guidata(hObject, handles);


function edit1_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function popupmenu1_Callback(hObject, eventdata, handles)

% Determine the selected data set.
str = get(hObject, 'String');
val = [150 300 1500];
% Set current data to the selected data set.
switch str{val};
case 'short' % User selects short.
   handles.totalTimePoints = val(1);
case 'medium' % User selects medium.
   handles.totalTimePoints = val(2);
case 'long' % User selects long.
   handles.totalTimePoints = val(3);
end
% Save the handles structure.
guidata(hObject,handles)



function popupmenu1_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton1_Callback(hObject, eventdata, handles)
simulation_attraction(hObject, eventdata, handles)
% Display surf plot of the currently selected data.
surf(handles.simulation_attraction);
  guidata(hObject, handles);

function text2_ButtonDownFcn(hObject, eventdata, handles)
