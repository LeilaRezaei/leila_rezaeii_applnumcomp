%% Computational assignmnet 4
% $k_12$, $k_13$, $k_14$, $k_23$ and $k_24$ are estimated by the function param_estim_4lump for the set
% of equations given below.
%
% $$ \frac{dy_1}{dt} = -(k_12+k_13+k_14)*y_1^2 $$
%
% $$ \frac{dy_2}{dt} = k_12*y_1^2-k_23*y_2-k_24*y_2 $$
%
% $$ \frac{dy_3}{dt} = k_13*y_1^2+k_23*y_2 $$
%
% $$ \frac{dy_4}{dt} = k_14*y_1^2+k_24*y_2 $$
%
% $y_1$, $y_2$, $y_3$ and $y_4$ are weight fractions of VGO, Gasoline, Gas and Coke.
%% Inputs
% initial conditions are y1(0) = 1, y2(0) = 0, y3(0) = 0, and y4(0) = 0, 
% values of time , weight fractions of each of the lumps,
% initial guesses for k_12, k_13, k_14, k_23 and k_24 
%conversion

%% Outputs
% The outputs are values of the three parameters and plots of actual yield
% and fitted yield against time and conversion
%% Function
function param_estim_4lump

% Given data for 3 lump model
xdata = [1/60, 1/30, 1/20, 1/10];      %time(h)
x1=[0.5074, 0.3796, 0.2882, 0.1762];
x2=[0.3767, 0.4385, 0.4865, 0.5416];
x3=[0.0885, 0.136, 0.1681, 0.2108];
x4=[0.0274, 0.0459, 0.0572, 0.0714];
ydata=[x1; x2; x3; x4];
conversion= [0.4926, 0.6204, 0.7118, 0.8238];
%% Guesses for parameters k1 & k2 & k3 
k(1)=50;
k(2)=11;
k(3)=5;
k(4)=1;
k(5)=1;
params_guess =k;
%% Initial conditions for ODEs 
x0(1)=1;
x0(2)=0;
x0(3)=0;
x0(4)=0;
%% Estimate parameters

[params,resnorm,residuals,exitflag,output]=lsqcurvefit(@(params,xdata) ...
    ODEmodel(params,xdata,x0),params_guess,xdata,ydata, [], [])

% try with x0 as the first entry in xdata,ydata

%%
%(y-t)& (y-conversation) plots
xdata = [1/60, 1/30, 1/20, 1/10];      %time(h)
x1=[0.5074, 0.3796, 0.2882, 0.1762];
x2=[0.3767, 0.4385, 0.4865, 0.5416];
x3=[0.0885, 0.136, 0.1681, 0.2108];
x4=[0.0274, 0.0459, 0.0572, 0.0714];
ydata=[x1; x2; x3; x4];
conversion= [0.4926, 0.6204, 0.7118, 0.8238];

% plots 
figure(1)
hold on
% VGO
plot(xdata,ydata(1,:),'rx')
xlabel('Time(hours)')
ylabel('yield , wt fraction')
% Gasoline
plot(xdata,ydata(2,:),'b+')
% Gas
plot(xdata,ydata(3,:),'o')
% Coke
plot(xdata,ydata(3,:),'*')
y_calc = ODEmodel(params,xdata,x0);
plot(xdata, y_calc(1,:),'r-o')
plot(xdata, y_calc(2,:),'b-d')
plot(xdata, y_calc(3,:))
plot(xdata, y_calc(4,:))
hold off
legend('VGO data','Gasoline data','Gas data','Coke data','VGO fit','Gasoline fit','Gas fit', 'Coke fit')

figure(2)
hold on
%VGO
plot(conversion,ydata(1,:),'s')
xlabel('Conversion, wt fraction')
ylabel('yield , wt fraction')
%Gasoline
plot(conversion,ydata(2,:),'x')
% Gas
plot(conversion,ydata(3,:),'o')
% Coke
plot(conversion,ydata(4,:),'*')
y_calc=ODEmodel(params,xdata,x0);
plot(conversion, y_calc(1,:),'b')
plot(conversion, y_calc(2,:),'r')
plot(conversion,y_calc(3,:))
plot(conversion,y_calc(4,:))
hold off
legend('VGO data','Gasoline data','Gas data','Coke data','VGO fit','Gasoline fit','Gas fit', 'Coke fit')

%% 
% ODEs of the three-lump model
% by using parameters = [k_12, k_13, k_14,k_23, k_24 ]
function dydt = ODE2(t,y,params)
    k_12=params(1);
    k_13=params(2);
    k_14=params(3);
    k_23=params(4);
    k_24=params(5);
    dydt(1)=-(k_12+k_13+k_14)*y(1)^2;
    dydt(2)=k_12*y(1)^2-k_23*y(2)-k_24*y(2);
    dydt(3)=k_13*y(1)^2+k_23*y(2);
    dydt(4)=k_14*y(1)^2+k_24*y(2);
    dydt=dydt';
end

%% 
% Solving ODEs:
function y_output = ODEmodel(params,timedata,y0)
    for i = 1:length(timedata); 
        tspan = [0:0.01:timedata(i)];
        [~,y_calc] = ode23s(@(t,y) ODE2(t,y,params),tspan,y0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output=y_output';
end
end

