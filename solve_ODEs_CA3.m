
%% solve the first order system of equations using ode45
function solve_ODEs_CA3
t=[423,100,0,0];                 % initial values
[x,t]=ode45(@ODEs_CA3,[0:0.01:1],t) 
%% plot
plot(x,t(:,1),'r-'),xlabel('V(dm^3)') 
hold on
plot(x,t(:,2),'g--'),xlabel('V(dm^3)')
hold on
plot(x,t(:,3),'c*'),xlabel('V(dm^3)')
hold on
plot(x,t(:,4),'b-.'),xlabel('V(dm^3)')
legend('T(k)','F_A','F_B','F_C')
function output=ODEs_CA3(x,t)
 %% define variables
T=t(1);
F_A=t(2);
F_B=t(3);
F_C=t(4);

CP_A=90;           % J/mol.C
CP_B=90;           % J/mol.C
CP_C=180;          % J/mol.C
T_a=373;           % K
T0=423;            % K
U_a=4000;          %J/m^3.s.C
C_T0=0.1;
deltaR_1A=-20000;  % J/(mol of A reacted in reaction 1)
deltaR_2A=-60000;  % J/(mol of A reacted in reaction 2)
E1_R=4000;         % E1_R=E1/R, K
E2_R=9000;         % E2_R=E2/R, K
F_T=F_A+F_B+F_C;
k_1A=10*exp(E1_R*(1/300-1/T));
k_2A=0.09*exp(E2_R*(1/300-1/T));
C_A=C_T0*(F_A/F_T)*(T0/T);
C_B=C_T0*(F_B/F_T)*(T0/T);
C_C=C_T0*(F_C/F_T)*(T0/T);
r_1A=-k_1A*C_A;
r_2A=-k_2A*C_A^2;
r_A=-k_1A*C_A-k_2A*C_A^2;
r_B=k_1A*C_A;
r_C=0.5*k_2A*C_A^2;
%% define equations
dTdt=(U_a*(T_a-T)+r_1A*deltaR_1A+r_2A*deltaR_2A)/(F_A*CP_A+F_B*CP_B+F_C*CP_C);
dFAdt=r_A;
dFBdt=r_B;
dFCdt=r_C;
output=[dTdt;dFAdt;dFBdt;dFCdt];
hold on

end
end
