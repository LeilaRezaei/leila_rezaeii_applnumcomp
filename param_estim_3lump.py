"""

@author: Leila Rezaei
Python parameter estimation-Computational assignmnet 4
 $k_1$, $k_2$ and $k_3$ parameters are estimated by the function param_estim_3lump for the set
 of equations given below.
 $$ \frac{dy_1}{dt} = -(k_1+k_3)*y_1^2 $$

 $$ \frac{dy_2}{dt} = k_1*y_1^2-k_2*y_2 $$

 $$ \frac{dy_3}{dt} = k_3*y_1^2+k_2*y_2 $$
  
"""

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import odeint
import matplotlib.pyplot as plt
# Data for three-lump model
xaxisData = np.array( [1/60, 1/30, 1/20, 1/10] ) # time(h) 
yaxisData = np.array( [ [0.5074, 0.3796, 0.2882, 0.1762], [0.3767, 0.4385, 0.4865, 0.5416],[0.1159, 0.1819, 0.2253, 0.2822] ] ) # % VGO, Gasoline, Gas+Coke 
conversion = np.array([0.4926, 0.6204, 0.7118, 0.8238])
# Initial guesses for parameters k1 & k2 & k3 
k1guess=0;
k2guess=1;
k3guess=1; 
parameterGuesses = np.array([k1guess,k2guess,k3guess])

#Define ODEs for three-lump model
# Uses params = [k_1, k_2, k_3]
def ODE_definition(x,t,args): #deriv (yvar, xvar,arguments)
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    k1 = args[0]
    k2 = args[1]
    k3 = args[2]
    dx1dt = -(k1+k3)*x1**2;
    dx2dt = k1*x1**2-k2*x2;
    dx3dt = k3*x1**2+k2*x2;
    return dx1dt, dx2dt, dx3dt

# Define the model for three-lump model, which requires solution to a system of differential equations
# Uses current params values and xaxisData as the final point in the tspan for the ODE solver
def ODEmodel(xaxisData,*params):
    #Initial condition for ODE in three-lump model
    yaxis0 = np.array([1,0,0])
    numYaxisVariables = 3 
    yaxisOutput = np.zeros((xaxisData.size,numYaxisVariables)) 
    for i in np.arange(0,len(xaxisData)):
        xaxisIncrement = 0.001 # adjust as needed for your problem
        xaxisSpan = np.arange(0,xaxisData[i]+xaxisIncrement,xaxisIncrement)
        y_calc = odeint(ODE_definition,yaxis0,xaxisSpan,args=(params,)) 
        yaxisOutput[i,:]=y_calc[-1,:]# UPDATE: this is now 2D with the number of columns set as : to include all yvariables
    # UPDATE: at this point we have a 2D matrix of yaxisOutput values. curve_fit 
    # needs a 1D vector that has the rows in a certain order, which result from the next two commands
    yaxisOutput = np.transpose(yaxisOutput)
    yaxisOutput = np.ravel(yaxisOutput)
    return yaxisOutput

#Estimate parameters for three-lump model
parametersoln, pcov = curve_fit(ODEmodel,xaxisData,np.ravel(yaxisData),p0=parameterGuesses)


plt.subplot(211)
plt.plot(xaxisData, yaxisData[0,:],'o') 
plt.plot(xaxisData, yaxisData[1,:],'x')
plt.plot(xaxisData, yaxisData[2,:],'*')
yaxis0 = np.array([1,0,0])
numYaxisVariables = 3

xforPlotting = np.linspace(0,xaxisData[-1],100) # this creates 100 xaxis points between 0 and the final xaxisData point to be used for plotting a smooth function
y_calculated = ODEmodel(xforPlotting,*parametersoln)
y_calculated = np.reshape(y_calculated,(numYaxisVariables,xforPlotting.size))
plt.plot(xforPlotting, y_calculated[0,:],'-', color='green', label='$VGO$') 
plt.plot(xforPlotting, y_calculated[1,:],'-.',color='orange', label='$Gasoline$') 
plt.plot(xforPlotting, y_calculated[2,:],'-.' ,color='blue', label='$Gas+Coke') 
plt.legend()
conversion = 1 - y_calculated[0,:]
plt.ylabel('Yield wt fraction')
plt.xlabel('$time (h)$')
plt.show()


print('b1 =',parametersoln[0])
print(parametersoln)
print(pcov)
