import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# function that returns dz/dv

def solve_ODEs_CA3():
    
    def ODEs_CA3(z,v):
       T=z[0]
       F_A=z[1]
       F_B=z[2]
       F_C=z[3]

       CP_A=90           
       CP_B=90           
       CP_C=180          
       T_a=373           
       T0=423
       U_a=4000          
       C_T0=0.1
       deltaR_1A=-20000     # J/(mol of A reacted in reaction 1)
       deltaR_2A=-60000     # J/(mol of A reacted in reaction 2)
       E1_R=4000            # E1_R=E1/R, K
       E2_R=9000            # E2_R=E2/R, K
       F_T=F_A+F_B+F_C
       k_1A=10*np.exp(E1_R*(1/300-1/T))
       k_2A=0.09*np.exp(E2_R*(1/300-1/T))
       C_A=C_T0*(F_A/F_T)*(T0/T)
       C_B=C_T0*(F_B/F_T)*(T0/T)
       C_C=C_T0*(F_C/F_T)*(T0/T)
       r_1A=-k_1A*C_A
       r_2A=-k_2A*C_A**2
       r_A=-k_1A*C_A-k_2A*C_A**2
       r_B=k_1A*C_A
       r_C=0.5*k_2A*C_A**2
    
       dTdv=(U_a*(T_a-T)+r_1A*deltaR_1A+r_2A*deltaR_2A)/(F_A*CP_A+F_B*CP_B+F_C*CP_C)
       dFAdv=r_A
       dFBdv=r_B
       dFCdv=r_C
       dzdv =[dTdv,dFAdv,dFBdv,dFCdv]
       return dzdv

    # initial condition
    z0 = [423,100,0,0]

    # v points
    v = np.linspace(0,1,101)

    # solve ODE
    output = odeint(ODEs_CA3,z0,v)
    # output vectors
    z1 = output[:,0]
    z2 = output[:,1]
    z3 = output[:,2]
    z4 = output[:,3]

# plot results

    plt.subplot(211)
    line1 = plt.plot(v, z1, '-', color='green', label='$T$')
    plt.subplot(212)
    line2 = plt.plot(v, z2, '-.',color='orange', label='$F_A$')
    line3 = plt.plot(v, z3, '.',color='blue', label='$F_B$')
    line3 = plt.plot(v, z4, '--',color='black', label='$F_C$')
    plt.legend()
    plt.ylabel('$A-B$')
    plt.xlabel('$x$')
    plt.show()
    return

solve = solve_ODEs_CA3()
